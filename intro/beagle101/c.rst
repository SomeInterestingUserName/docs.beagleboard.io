.. _intro-c:

Introduction to C
#################

.. todo:: Place-holder for tutorial on the C programming language

The C programming language is powerful and flexible and sits at the heart of
many key open source software projects, including :ref:`Linux <intro-linux>` and
:ref:`Zephyr <intro-zephyr>`. While it is intimidating to many people, C holds a
strong relevance in today's technology landscape and can be approached and learned
by those without extensive previous computer knowledge.

While there are many :ref:`resources <intro-c-resources>` for learning C, this article
will focus primarily on learning C using Linux and specifically a Beagle running
:ref:`Debian GNU/Linux <intro-debian>`. It is recommended to utilize on-line learning
resources like `learn-c.org`_ to get familiarity with the language, then apply what you
have learned on a Beagle to see it in real life.

.. note::

   Making the transition from on-line to in-person compilation and execution of your
   code is critical to building and maintaining your understanding. You can never be
   certain that an on-line service will always be available, so do yourself the favor
   or transitioning your exercises and materials into your own possession as soon as
   it makes sense for you.

Why Learn C?
************

While C is an extremely popular programming language, popularity shouldn't necessarily be
your top guide in choosing a programming language to learn. What are some of the best
reasons to learn C?

Provides Strong Foundation
==========================

Many other programming languages are very similar to C, like `Java`_, `Wiring`_ or `Rust`_,
are direct extensions of C, namely `C++`_ and `C#`_, or are written in C themselves.
Learning C will set you up well to learn many programming languages and the rationales
behind them.

Your First C Program
********************

:ref:`start-browse-to-beagle`

:ref:`intro-using-vs-code`

.. _intro-c-resources:

Recommended Resources
*********************

* `C Programming Language, 2nd Edition <https://a.co/d/51eEGca>`_
* `learn-c.org`_

.. _C++:
   https://en.wikipedia.org/wiki/C%2B%2B

.. _Wiring:
   https://en.wikipedia.org/wiki/Wiring_(software)

.. _Java:
   https://en.wikipedia.org/wiki/Java_(programming_language)

.. _Rust:
   https://en.wikipedia.org/wiki/Rust_(programming_language)

.. _learn-c.org:
   https://learn-c.org

.. _C#:
   https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29
